[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

# docker-nodejs

Small image containing nodejs and npm.

Based on [mreil/ubuntu-base][ubuntu-base].


## Usage

    docker pull mreil/nodejs


## Build

    ./gradlew build


## Releases

See [hub.docker.com](https://hub.docker.com/r/mreil/nodejs/tags/).


## License

see [LICENSE](LICENSE)


[ubuntu-base]: https://bitbucket.org/mreil-com/docker-ubuntu-base
