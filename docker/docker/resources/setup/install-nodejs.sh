#!/usr/bin/env bash

# Install packages
apt-get update -y
apt-get install -y curl python-software-properties


# Install LTS release
curl -sL https://deb.nodesource.com/setup_8.x | bash -
apt-get install -y nodejs


# Uninstall unneeded stuff
apt-get remove -y curl python-software-properties


### Output version
nodejs --version
